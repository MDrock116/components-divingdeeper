import React, {PureComponent} from "react";
import Person from "./Person/Person";
import PropTypes from 'prop-types'

class Persons extends PureComponent{
    // Commented out because not commonly used and causes warning
    // static getDerivedStateFromProps(props, state){
    //     console.log('[Persons.js] getDerivedSateFromProps ')
    //     return state;
    // }

    // shouldComponentUpdate(nextProps, nextState, nextContext) {
    //     console.log('[Persons.js] shouldComponentUpdate ')
    //     return nextProps.persons !== this.props.persons || //shallow comparison of array but works since we followed rules & a new array is created when changing state
    //         nextProps.props.changed !== this.props.changed ||
    //         nextProps.props.clicked !== this.props.clicked;
    // }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('[Persons.js] getSnapshotBeforeUpdate ')
        return {message: 'Snapshot!'}  //used, for last minute DOM reads (like current scrolling position)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        console.log('[Persons.js] componentDidUpdate ')
        console.log(snapshot)
    }

    componentWillUnmount() {
        //Place you can remove any event listeners etc...
        console.log('[Persons.js] componentWillUnmount')
    }

    render(){
        console.log('[Persons.js] rendering... ');
        return(
            this.props.persons.map((person, index) =>
                <Person
                    click={() => this.props.clicked(index)}
                    name={person.name}
                    age={person.age}
                    key={person.id}
                    changed={event => this.props.changed(event, person.id)}
                />)
        )
    }
}

Person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changed: PropTypes.func
};

export default Persons;