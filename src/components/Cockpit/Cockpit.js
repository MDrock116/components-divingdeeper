import React, {useContext, useEffect, useRef} from "react";
import classes from './Cockpit.css';
import Authcontext from "../../context/auth-context";



const cockpit = (props) => {
    const toggleBtnRef = useRef(null);

    const authContext = useContext(Authcontext);

    console.log(authContext.authenticated);

    useEffect(() => { //runs on component creation & for every update
        console.log('[Cockpit.js] 2nd useEffect')
        return () => {
            console.log('[Cockpit.js] cleanup work in 2nd useEffect')
        }
    });

    useEffect(() => { //runs on component creation & for every update
        console.log('[Cockpit.js] useEffect')
        //http request...
        // setTimeout(()=>{
        //     alert('Saved data to Cloud!')
        // }, 1000)
        toggleBtnRef.current.click();  //exists in useEffect to allows JSX w/ ref to render first
        return () => {
            console.log('[Cockpit.js] cleanup work in useEffect')
        }
    },[]);

    let btnClass = '';
    if(props.showPersons){
        btnClass = classes.Red;
    }

    const assignedClasses = [];
    if (props.personsLength <= 2) {
        assignedClasses.push(classes.red); // classes = ['red']
    }
    if (props.personsLength <= 1) {
        assignedClasses.push(classes.bold); // classes = ['red', 'bold']
    }

    return(
        <div className={classes.Cockpit}>
            <h1>Hi, I'm a React App</h1>
            <p className={assignedClasses.join(' ')}>This is really working!</p>
            <button ref={toggleBtnRef} className={btnClass} onClick={props.clicked}>
                Toggle Persons
            </button>
                <button onClick={authContext.login}>Log in</button>
        </div>

    );
}

export default React.memo(cockpit);