import React from "react";

const authcontext = React.createContext({
    authenticated: false,
    login: () => {}  //only used to get better autocompletion from IDE
});

export default authcontext;